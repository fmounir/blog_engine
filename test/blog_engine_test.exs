defmodule BlogEngineTest do
  use ExUnit.Case , async: false
  import Mock
  use Plug.Test
  

  doctest BlogEngine

   test_with_mock "when opts is :index , index function is called" , BlogEngine.BlogController,[:passthrough] ,[] do
    conn = conn(:get , "/allposts")
    newconn = BlogEngine.BlogController.call(conn , :index)
  
    newconn.assigns[:function_called] == "index"
  end

   test_with_mock "when opts is any thing , default function is called" , BlogEngine.BlogController,[:passthrough] ,[] do
    conn = conn(:get , "/allposts")
    newconn = BlogEngine.BlogController.call(conn , :anything)
  
    newconn.assigns[:function_called] == "default"
  end
end
