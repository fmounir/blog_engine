# BlogEngine

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add blog_engine to your list of dependencies in `mix.exs`:

        def deps do
          [{:blog_engine, "~> 0.0.1"}]
        end

  2. Ensure blog_engine is started before your application:

        def application do
          [applications: [:blog_engine]]
        end

