defmodule BlogEngine do
defmacro __using__(_) do
    quote do 
        import BlogEngine.BlogController
    end
end
end
