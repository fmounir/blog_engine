defmodule BlogEngine.BlogController  do
import Plug.Conn
    def init(opts) do
        opts
    end

    def call(conn , opts) do 
        IO.inspect(conn)
        IO.inspect(opts)
        
        case(opts) do
            :index ->
               index(conn, opts)
            default ->
                default(conn)
        end
    end

    def index(conn , _opts) do       
       conn 
       |> Plug.Conn.put_resp_content_type("text/html")
       |> assign(:function_called, "index")
       |> IO.inspect   
    end

    def default(conn) do
       conn 
       |> Plug.Conn.put_resp_content_type("text/html")
       |> assign(:function_called, "default")
       |> IO.inspect   
    end

end